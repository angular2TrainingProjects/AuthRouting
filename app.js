"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const core_2 = require("@angular/core");
const platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
const platform_browser_1 = require("@angular/platform-browser");
const router_1 = require("@angular/router");
const core_3 = require("@angular/core");
const common_1 = require("@angular/common");
require('rxjs/Rx');
let AuthService = class AuthService {
    login(username, password) {
        if (username === "farhad" && password === "101") {
            localStorage.setItem("username", username);
            return true;
        }
        return false;
    }
    getUser() {
        return localStorage.getItem("username");
    }
    isLoggedIn() {
        return this.getUser() !== null;
    }
    logOut() {
        localStorage.removeItem("username");
    }
};
AuthService = __decorate([
    core_3.Injectable(), 
    __metadata('design:paramtypes', [])
], AuthService);
let LoginComponent = class LoginComponent {
    constructor(authService) {
        this.authService = authService;
        this.message = '';
    }
    login(username, pass) {
        if (!this.authService.login(username, pass)) {
            this.message == "credential is incorrect!";
            setTimeout(function () {
                this.message = ' ';
            }.bind(this), 2500);
        }
    }
    logout() {
        this.authService.logOut();
        return false;
    }
};
LoginComponent = __decorate([
    core_1.Component({
        selector: "login",
        template: `
          <div class="form-inline" *ngIf="!authService.isLoggedIn()">
               <div class="form-group" >
                    <label for="username">username:</label>
                    <input type="text" class="form-control" id="username" #user/>
               </div>
               <div class="form-group">
                    <label for="pass">password: </label>
                    <input type="password" class="form-control" id="pass" #pass/>
               </div>
               <div class="form-group">
                    <button class="btn btn-primary" (click)="login(user.value,pass.value)">submit</button>
               </div>
               <div class="alert alert-danger" role="alert" *ngIf="message">{{ message }}</div>
          </div>
          <div class="" *ngIf="authService.isLoggedIn()">
               <div class="well">
                    you are loged in as {{ authService.getUser() }} !
                    <a href="" (click)="logout()">log out</a>
               </div>
          </div>
          `
    }), 
    __metadata('design:paramtypes', [AuthService])
], LoginComponent);
let LoggedInGuard = class LoggedInGuard {
    constructor(authService) {
        this.authService = authService;
    }
    canActivate() {
        return this.authService.isLoggedIn();
    }
};
LoggedInGuard = __decorate([
    core_3.Injectable(), 
    __metadata('design:paramtypes', [AuthService])
], LoggedInGuard);
let LoginDiv = class LoginDiv {
    constructor(router) {
        this.router = router;
    }
};
LoginDiv = __decorate([
    core_1.Component({
        selector: "login-div",
        template: `
          <login></login>
          <div>
               <a [routerLink]="['home']">home</a> | 
               <a [routerLink]="['about']">about us</a> | 
               <a [routerLink]="['protected']">protected</a>
          </div>
          <hr/>
          <router-outlet></router-outlet>
          `
    }), 
    __metadata('design:paramtypes', [router_1.Router])
], LoginDiv);
let HomeComponent = class HomeComponent {
};
HomeComponent = __decorate([
    core_1.Component({
        selector: "home",
        template: `
            <div> <p> here is my home! </p> </div>
            `
    }), 
    __metadata('design:paramtypes', [])
], HomeComponent);
let AboutComponent = class AboutComponent {
};
AboutComponent = __decorate([
    core_1.Component({
        selector: "about",
        template: `
            <div> <p> here is my about ! </p> </div>
            `
    }), 
    __metadata('design:paramtypes', [])
], AboutComponent);
let ProtectedComponent = class ProtectedComponent {
};
ProtectedComponent = __decorate([
    core_1.Component({
        selector: "protected",
        template: `
            <div> <p> here is my protected! </p> </div>
            `
    }), 
    __metadata('design:paramtypes', [])
], ProtectedComponent);
const routes = [
    { path: ' ', redirectTo: 'home' },
    { path: "home", component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'protected', component: ProtectedComponent, canActivate: [LoggedInGuard] }
];
let loginForProtectedRoute = class loginForProtectedRoute {
};
loginForProtectedRoute = __decorate([
    core_2.NgModule({
        declarations: [LoginComponent, LoginDiv, HomeComponent, AboutComponent, ProtectedComponent],
        imports: [platform_browser_1.BrowserModule, router_1.RouterModule.forRoot(routes)],
        providers: [LoggedInGuard, AuthService, { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy }],
        bootstrap: [LoginDiv]
    }), 
    __metadata('design:paramtypes', [])
], loginForProtectedRoute);
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(loginForProtectedRoute);
//# sourceMappingURL=app.js.map