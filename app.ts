import { Component } from "@angular/core";
import { NgModule } from "@angular/core";
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { BrowserModule } from "@angular/platform-browser";
import {RouterModule,Routes,ActivatedRoute,Router,CanActivate } from "@angular/router";
import { Injectable,Inject } from "@angular/core";
import {LocationStrategy,HashLocationStrategy,APP_BASE_HREF,Location} from "@angular/common";
import {Observable} from "rxjs/Rx";
import 'rxjs/Rx';

@Injectable()     
class AuthService {
     login(username: string, password: string): boolean {
          if(username === "farhad" && password === "101") {
               localStorage.setItem("username",username);
               return true;
          }
          return false;
     }
     getUser(): string {
          return localStorage.getItem("username");
     }
     isLoggedIn(): boolean {
          return this.getUser() !== null;
     }
     logOut() {
          localStorage.removeItem("username");
     }
}
@Component({
     selector: "login",
     template: `
          <div class="form-inline" *ngIf="!authService.isLoggedIn()">
               <div class="form-group" >
                    <label for="username">username:</label>
                    <input type="text" class="form-control" id="username" #user/>
               </div>
               <div class="form-group">
                    <label for="pass">password: </label>
                    <input type="password" class="form-control" id="pass" #pass/>
               </div>
               <div class="form-group">
                    <button class="btn btn-primary" (click)="login(user.value,pass.value)">submit</button>
               </div>
               <div class="alert alert-danger" role="alert" *ngIf="message">{{ message }}</div>
          </div>
          <div class="" *ngIf="authService.isLoggedIn()">
               <div class="well">
                    you are loged in as {{ authService.getUser() }} !
                    <a href="" (click)="logout()">log out</a>
               </div>
          </div>
          `
})
class LoginComponent {
    message: string;
    constructor(public authService: AuthService) { 
         this.message = '';
    }
    login(username,pass) {
        if(!this.authService.login(username,pass)) {
             this.message == "credential is incorrect!";
             setTimeout(function() {
                  this.message = ' ';
             }.bind(this),2500);
        }
    }
    logout(): boolean {
         this.authService.logOut();
         return false;
    }
}
@Injectable()
class LoggedInGuard implements CanActivate {
     constructor(public authService: AuthService) { }
     canActivate():boolean {
          return this.authService.isLoggedIn();
     }
}

@Component({
     selector: "login-div",
     template: `
          <login></login>
          <div>
               <a [routerLink]="['home']">home</a> | 
               <a [routerLink]="['about']">about us</a> | 
               <a [routerLink]="['protected']">protected</a>
          </div>
          <hr/>
          <router-outlet></router-outlet>
          `
})
class LoginDiv {
     constructor(public router: Router) {}
}
@Component({
    selector: "home",
    template: `
            <div> <p> here is my home! </p> </div>
            `
          })
class HomeComponent {}
@Component({
    selector: "about",
    template: `
            <div> <p> here is my about ! </p> </div>
            `
          })
class AboutComponent {}
@Component({
    selector: "protected",
    template: `
            <div> <p> here is my protected! </p> </div>
            `
          })
class ProtectedComponent {}
const routes: Routes = [
      {path: ' ', redirectTo: 'home' },
     {path: "home", component: HomeComponent},
     {path: 'about', component: AboutComponent},
     {path: 'protected', component: ProtectedComponent, canActivate: [LoggedInGuard]}
     ];

@NgModule({
     declarations: [LoginComponent,LoginDiv,HomeComponent,AboutComponent,ProtectedComponent],
     imports: [BrowserModule,RouterModule.forRoot(routes)],
     providers: [LoggedInGuard,AuthService,{provide: LocationStrategy, useClass: HashLocationStrategy}],
     bootstrap: [LoginDiv]
})
class loginForProtectedRoute {}
platformBrowserDynamic().bootstrapModule(loginForProtectedRoute);
